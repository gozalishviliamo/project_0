FROM python:latest

RUN mkdir /usr/src/app/
WORKDIR /usr/src/app/

COPY . /usr/src/app/
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

RUN pip install dist/wheel_pack-1.0-py2.py3-none-any.whl

RUN cd wheel_pack

EXPOSE 8089

CMD ["python","app.py"]