---
title: file
---
<SwmSnippet path="/2-igw.tf" line="1">

---

This code snippet creates an AWS internet gateway resource in a specific VPC, and assigns a name tag to it.

```tf
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "igw"
  }
}
```

---

</SwmSnippet>

<SwmMeta version="3.0.0" repo-id="Z2l0bGFiJTNBJTNBcHJvamVjdF8wJTNBJTNBZ296YWxpc2h2aWxpYW1v" repo-name="project_0"><sup>Powered by [Swimm](https://app.swimm.io/)</sup></SwmMeta>
