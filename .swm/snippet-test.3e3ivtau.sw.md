---
title: snippet-test
---
<SwmSnippet path="/0-provider.tf" line="1">

---

This code snippet is a Terraform configuration file. It defines the required provider 'aws' with the source 'hashicorp/aws' and version '\~> 4.0'. It also configures the AWS provider with the region, access key, and secret key provided as variables.

```tf
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region     = "${var.region}"
  access_key = "${var.aws_access_key}" 
  secret_key = "${var.aws_secret_key}" 
}

```

---

</SwmSnippet>

<SwmMeta version="3.0.0" repo-id="Z2l0bGFiJTNBJTNBcHJvamVjdF8wJTNBJTNBZ296YWxpc2h2aWxpYW1v" repo-name="project_0"><sup>Powered by [Swimm](https://app.swimm.io/)</sup></SwmMeta>
