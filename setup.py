from setuptools import setup

setup(name="wheel_pack",
        version="1.0",
        packages=['.wheel_pack'],
        options={"bdist_wheel":{"universal":True}}
    )