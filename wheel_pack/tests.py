from wheel_pack.__init__ import app, db
import pytest

data = {
        'name': 'Item 1',
        'price': 10.99
        }

@pytest.fixture
def client():
    # app = create_app()
    app.config['TESTING'] = True
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
    with app.test_client() as client:
        with app.app_context():
            db.create_all()
        yield client
        with app.app_context():
            db.drop_all()


def test_get_item(client):
    client.post('/items', json=data)
    response = client.get('/items')
    # response = client.get('/items/1')
    print(response.status_code)
    print(response.get_data())  # Print the response content
    assert response.status_code == 200
    assert response.data

    # assert response.json['name'] == 'Item 1'
    # assert response.json['price'] == 10.99
    # assert response.json() == {"name": "item 1", 'price': 10.99 }

def test_create_item(client):
    # Test creating an item using POST request

    response = client.post('/items', json=data)
    print(response.status_code)
    print(client)
    print(response)
    print(response.get_data())
    assert response.status_code == 201
    assert response.data

    # assert response.json['name'] == 'Item 1'
    # assert response.json['price'] == 10.99
    # assert response.json() == {"name": "item 1", 'price': 10.99 }

def test_read_items(client):
    # Test reading an item using GET request
    client.post('/items', json=data)
    # client.get('/items/1')
    response = client.get('/items/1')
    print(response.status_code)
    print(response.get_data())
    assert response.status_code == 200
    assert response.data

updated_data = {
                'name': 'Updated Item',
                'price': 15.99
                }

def test_update_item(client):
    # Test updating an item using PUT request
    client.post('/items', json=data)
    response = client.put('/items/1', json=updated_data)
    client.get('items')
    print(response.status_code)
    print(response.get_data())
    assert response.status_code == 200
    assert response.data
    assert response.json['name'] == 'Updated Item'
    assert response.json['price'] == 15.99

def test_delete_item(client):
    # Test deleting an item using DELETE request
    client.post('/items', json=data)

    response = client.delete('/items/1')
    print(response.status_code)
    assert response.status_code == 200
    assert response.data

def test_list_items(client):
    item1 = {'name': 'Item 1', 'price': 10.99}
    item2 = {'name': 'Item 2', 'price': 20.99}
    client.post('items', json=item1)
    client.post('items', json=item2)

    response = client.get('/items')
    print(response.status_code)
    print(response.get_data())
    assert response.status_code == 200
    assert response.data[0]
    assert response.data[1]
