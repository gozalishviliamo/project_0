from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Resource, reqparse

app = Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://postgres:testpassword@10.107.145.86:54321/postgres"
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
db = SQLAlchemy(app)
api = Api(app)

class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)
    price = db.Column(db.Float, nullable=False)

    def __init__(self, name, price):
        self.name = name
        self.price = price

    def to_dict(self):
        return {"id": self.id, "name": self.name, "price": self.price}

# # Initialize the database
with app.app_context():
    db.create_all()

# Request parser for parsing JSON data
parser = reqparse.RequestParser()
parser.add_argument("id", type=int, required=False, help="id field not required.")
parser.add_argument("name", type=str, required=True, help="Name field is required.")
parser.add_argument("price", type=float, required=True, help="Price field is required.")

class ItemResource(Resource):
    def get(self, item_id):
        item = Item.query.get(item_id)
        if item:
            return item.to_dict(), 200  # Set the response content type to JSON
        else:
            return {"message": "Item not found"}, 404


    def put(self, item_id):
        item = Item.query.get(item_id)
        if item:
            args = parser.parse_args()
            item.name = args['name']
            item.price = args['price']
            db.session.commit()
            return item.to_dict(), 200
        else:
            return {"message": "Item not found"}, 404

    def delete(self, item_id):
        item = Item.query.get(item_id)
        if item:
            db.session.delete(item)
            db.session.commit()
            return {"message": "Item deleted"}, 200
        else:
            return {"message": "Item not found"}, 404

class ItemListResource(Resource):
    def get(self):
        items = Item.query.all()
        return [item.to_dict() for item in items], 200

    def post(self):
        args = parser.parse_args()
        item = Item(args['name'], args['price'])
        db.session.add(item)
        db.session.commit()
        if item:
            return item.to_dict(), 201  # Set the response content type to JSON
        else:
            return {"message": "No Item created"}, 404


api.add_resource(ItemResource, '/items/<int:item_id>')
api.add_resource(ItemListResource, '/items')
