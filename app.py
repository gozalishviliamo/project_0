import json
from flask import jsonify
from wheel_pack.__init__ import app
from flask_swagger_ui import get_swaggerui_blueprint


# Swagger UI setup
SWAGGER_URL = '/swagger'
API_URL = '/swagger.json'
swaggerui_blueprint = get_swaggerui_blueprint(SWAGGER_URL, API_URL, config={'app_name': 'CRUD API'})
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)


# Swagger documentation
@app.route('/swagger.json', methods=['GET'])
def get_swagger():
    try:
        # Read the content of the external JSON file
        with open('swagger.json', 'r') as f:
            swagger_json = json.load(f)

        # Return the content as a JSON response
        return jsonify(swagger_json)

    except FileNotFoundError:
        # If the file is not found, return an error response
        error_message = {"error": "Swagger file not found"}
        return jsonify(error_message), 404

    except json.JSONDecodeError:
        # If there is an issue with decoding JSON, return an error response
        error_message = {"error": "Invalid JSON format in the Swagger file"}
        return jsonify(error_message), 500


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8089, debug=True)

