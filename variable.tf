variable "region" {
    default = "eu-central-1"
}


# access keys
variable "aws_access_key" {
default = "" #"${AWS_ACCESS_KEY_ID}"
}

variable "aws_secret_key" {
default = ""  #"${AWS_SECRET_ACCESS_KEY}"
}

variable "gitlab_access_token" {
default = ""
}

# cluster conf
variable "cluster_name" {
  default = "devops_eks_cluster"
}

variable "cluster_version" {
  default = "1.25"
}

# VPC conf
variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "vpc_tag" {
  default = "main_vpc"
}


# nodes conf
variable "instance_name" {
  default = "t3.xlarge"
}

# node group
variable "node_name" {
  default = "eks-node-group-nodes1"
}



# gitlab state file conf 


# example_remote_state_address = "https://gitlab.com/api/v4/projects/<TARGET-PROJECT-ID>/terraform/state/<TARGET-STATE-NAME>"
# example_username = "<GitLab username>"
# example_access_token = "<GitLab Personal Access Token>"
